from django import forms

from .models import Invite

class InviteForm(forms.Form):
    email = forms.EmailField(required=True, label='friend email')

    class Meta:
        model = Invite