from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context

from reviews.models import Review
from .models import Invite

from .forms import InviteForm

def send_invite(request, invite, review):

    try:
        plaintext = get_template('invite_email.txt')
        htmly = get_template('invite_email.html')
        
        subject = 'Reviewit Team :: Invite to join review'
        link = request.build_absolute_uri(reverse('reviews:view', args=(review.id, ))) + '?code=' + invite.code
        
        d = Context({
            'link'      : link,
            'username'  : review.reviewee.username,
            'title'     : review.title
        })

        text_content = plaintext.render(d)
        html_content = htmly.render(d)

        from_email = 'reviewit.co.in@gmail.com'

        msg = EmailMultiAlternatives(subject, text_content, from_email, [invite.email])
        msg.attach_alternative(html_content, "text/html")
        print 'invite sent!'
        msg.send()

        return True
    except:
        return False

@login_required
def add(request, review_id):

    if request.method == 'POST':
        form = InviteForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            email = cd['email']
            
            review = Review.objects.get(pk=review_id)

            try:
                Invite.objects.get(email=email, review=review)
            except:
                invite = Invite.objects.create_invite(email, review)

                send_invite(request, invite, review)

                response = {'invite': invite}
                if not request.is_ajax():
                    return redirect(reverse('reviews:view'), args=(review_id, ))
                return render(request, 'invite.html', response)
            else:
                return HttpResponse(status=400)

        else:
            print 'invalid form!'
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=400)