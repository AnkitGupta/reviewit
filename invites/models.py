from django.db import models

from reviews.models import Review
from .managers import InviteManager

class Invite(models.Model):
    email = models.CharField(max_length=200)
    review = models.ForeignKey(Review)
    code = models.CharField(max_length=200)
    objects = InviteManager()

    def __unicode__(self):
        return self.email

    def add_reviewers(self, user):
    	# Adds a user to the participants list of a review and removes the
        # entry from invites
    	self.review.reviewers.add(user)
        self.delete()