from django.db import models
import time, random, hashlib

class InviteManager(models.Manager):

    def _unique_code(self, tag):
        # Creates an md5 hash, unique hash based on the email id of the user
        # as an invitation code
        t1 = time.time()
        base = hashlib.md5(tag + str(t1))
        code = tag + '_' + base.hexdigest()
        return code

    def create_invite(self, email, review):
        # Creates an invite      
        invite = self.model(email=email, code=self._unique_code(email), review=review)
        invite.save(using=self._db)
        return invite