from django.conf.urls import patterns, url

from invites import views

urlpatterns = patterns('',
    
   url(
        r'^add/(?P<review_id>\d+)/$',
        views.add, 
        name='add'
   ),

)