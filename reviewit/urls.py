from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(
        r'^', 
        include(
            'reviews.urls', 
            namespace="reviews"
        )
    ),

    url(
        r'^accounts/', 
        include(
            'allauth.urls', 
            namespace="allauth"
        )
    ),

    url(
        r'^comment/', 
        include(
            'comments.urls', 
            namespace="comments"
        )
    ),

    url(
        r'^link/', 
        include(
            'links.urls', 
            namespace="links"
        )
    ),

    url(
        r'^invite/', 
        include(
            'invites.urls', 
            namespace="invites"
        )
    ),

    url(
        r'^admin/', 
        include(
            admin.site.urls
        )
    ),

    url(
        r'^terms/', 
        TemplateView.as_view(template_name="terms.html"),
        name='terms'
    ),

    url(
        r'^privacy/', 
        TemplateView.as_view(template_name="privacy.html"),
        name='privacy'
    ),

    url(
        r'^about/', 
        TemplateView.as_view(template_name="about.html"),
        name='about'
    ),

    url(
        r'^careers/', 
        TemplateView.as_view(template_name="career.html"),
        name='careers'
    ),
)
