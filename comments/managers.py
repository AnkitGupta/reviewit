from django.db import models
from django.utils import timezone

class CommentManager(models.Manager):

	def add_comment(self, detail, user, review):
		#Creates and saves comment
		now = timezone.now()		
		comment = self.model(detail=detail, user=user, review=review, date=now)
		comment.save(using=self._db)
		return comment