from django.conf.urls import patterns, url

from comments import views

urlpatterns = patterns('',
   
   url(
   		r'^add/(?P<review_id>\d+)/$',
   		views.add, 
   		name='add'
   ),

)