from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.contrib.auth.models import User

from .models import Comment
from reviews.models import Review

from .forms import CommentForm

def add(request, review_id):

	if request.user.is_authenticated():
		user = request.user
	else:
		user = User.objects.get(username='anonymous')	

	if request.method == 'POST':
		form = CommentForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			comment_detail = cd['detail']
			
			review = Review.objects.get(pk=review_id)
			comment = Comment.objects.add_comment(comment_detail, user, review)		

			if not request.is_ajax():
				return redirect(reverse('reviews:view'), args=(review_id, ))
		else:
			return HttpResponse(status=400)

	else:
		return HttpResponse(status=400)

	response = {'comment': comment}
	return render(request, 'comment.html', response)


