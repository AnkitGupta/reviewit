from django.db import models
from django.contrib.auth.models import User

from .managers import CommentManager
from reviews.models import Review

class Comment(models.Model):
    detail = models.CharField(max_length=10000)
    rate = models.IntegerField(default=0)
    review = models.ForeignKey(Review, related_name='comments')
    user = models.ForeignKey(User)
    date = models.DateTimeField()

    objects = CommentManager()

    def __unicode__(self):
        return self.description
