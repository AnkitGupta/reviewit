from django import forms

from .models import Comment

class CommentForm(forms.Form):
	detail = forms.CharField(required=True, widget=forms.Textarea, label='add review below')

	class Meta:
		model = Comment