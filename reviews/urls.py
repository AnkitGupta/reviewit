from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns

from reviews import views

urlpatterns = patterns('',
    
    url(
    	r'^$', 
    	views.index, 
    	name='index'
    ),

    url(
    	r'^ask/$', 
    	views.ask, 
    	name='ask'
    ),

    url(
    	r'^view/(?P<review_id>\d+)/$', 
    	views.view, 
    	name='view'
    ),
    
    url(
    	r'^list/$', 
    	views.list, 
    	name='list'
    ),

    url(
    	r'^list/(?P<username>[a-zA-Z0-9]+)/$', 
    	views.list, 
    	name='browse'
    ),

    url(
        r'^search/$', 
        views.search, 
        name='search'
    ),

    url(
        r'^feedback/$', 
        views.feedback, 
        name='feedback'
    ),

    url(
        r'^(?P<page>[-\w]+.html)/$',
        views.angular_views,
        name='angular_views'
    ),
)

urlpatterns = format_suffix_patterns(urlpatterns)