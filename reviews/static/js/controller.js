reviewsApp

/*
 listController:
    - Controller for listing all reviews
 */
.controller('listController',
[
    '$scope',
    'listService',

    function($scope, listService) {
        listService.get().then(function(result) {
            $scope.reviews = result;
        });
    }
])

/*
 detailController:
    - Controller for showing details of a particular review
 */
.controller('detailController',
[
    '$scope',
    '$routeParams',
    'listService',

    function($scope, $routeParams, listService) {
        listService.get($routeParams.id).then(function(result) {
            $scope.review = result;
        });
    }
]);
