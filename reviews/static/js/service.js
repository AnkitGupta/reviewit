reviewsApp

/*
 listService retrieves:
    - list of all reviews
    - detail of a review (by reviewId)
 */
.service('listService',
[
    '$http',
    '$q',

    function($http, $q) {
        this.get = function(reviewId) {
            var url = (reviewId !== undefined) ? '/view/' + reviewId : '/list/';
            var defer = $q.defer();

            $http.get(url).then(function(result) {
                defer.resolve(result.data);
            });

            return defer.promise;
        };
    }
]);
