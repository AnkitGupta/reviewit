reviewsApp

.config(
[
    '$routeProvider',

    function($routeProvider) {
        $routeProvider
        .when('/', {
            controller: 'listController',
            templateUrl: 'list.html'
        })
        .when('/view/:id', {
            controller: 'detailController',
            templateUrl: 'detail.html'
        })
        .otherwise({
            redirectTo: '/'
        });
    }
]);