from django.utils import timezone
from django.db import models

from utils.helper import get_query

class ReviewManager(models.Manager):

	def create_review(self, title, link, query, user):
		#Creates and saves a Review by a User
		now = timezone.now()
		review = self.model(title=title, link=link, query=query, reviewee=user, date=now)

		review.save(using=self._db)
		return review

	def get_all_reviews(self, user):
		# Get all reviews in which user is reviewee or reviewer 
		reviewee_Q =  models.Q(reviewee=user)
		reviewers_Q = models.Q(reviewers=user)

		return self.model.objects.filter(reviewee_Q | reviewers_Q).distinct()

	def search_all(self, query_string):
		# Get all reviews matching the query_string
		entry_query = get_query(query_string, ['title', 'query',])

		return self.model.objects.filter(entry_query)
