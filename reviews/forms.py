from django import forms

from .models import Review

class ReviewForm(forms.Form):
	title = forms.CharField(required=True, label='title')
	link = forms.URLField(required=True, label='product link')
	query = forms.CharField(required=True, widget=forms.Textarea, label='product queries')

	class Meta:
		model = Review 
		fields = ("title", "link", "query")