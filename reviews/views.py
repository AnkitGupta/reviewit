from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from links.models import Link
from invites.models import Invite
from comments.models import Comment
from .models import Review

from allauth.forms import AuthenticateForm
from comments.forms import CommentForm
from invites.forms import InviteForm
from .forms import ReviewForm

# Rest Framework specific imports
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from .serializers import ReviewSerializer

def index(request):
    return render(request, 'reviewsManager.html')

def angular_views(request, page):
    return render(request, page)

@csrf_protect
@login_required
def ask(request):
    """
    Creates a review and saves
    """
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            title = cd['title']
            query = cd['query']
            link  = cd['link']
            
            link_obj = Link.objects.create_link(link)
            review = Review.objects.create_review(title, link_obj, query, request.user)

            return redirect(
                reverse(
                    'reviews:view', 
                    args=(
                        review.id,
                    )
                )
            )
    else:
        form = ReviewForm()

    response = {'form': form}
    return render(request, 'ask.html', response)

@csrf_protect
def feedback(request):
    """
    Feedback for the app
    """
    admin = User.objects.get(username='admin')

    title = 'review reviewit app'
    link = Link.objects.create_link('http://reviewit.co.in')
    query = 'Please provide feedback on the reviewit app'

    try:
        review = Review.objects.get(reviewee=admin)
    except MultipleObjectsReturned:
        review = Review.objects.filter(reviewee=admin)[0]
    except ObjectDoesNotExist.DoesNotExist:
        review = Review.objects.create_review(title, link, query, admin)
    
    return redirect(reverse('reviews:view', args=(review.id, )))

@api_view(['GET'])
def view(request, review_id):
    review = get_object_or_404(Review, pk=review_id)

    serializer = ReviewSerializer(review)
    return Response(serializer.data)

@api_view(['GET'])
def list(request, username='anonymous', format=None):
    user = get_object_or_404(User, username=username)
    
    if request.user.is_anonymous() or username == 'anonymous':
        request.session['next_page'] = reverse('reviews:list')
        reviews = Review.objects.all().order_by('-date')
    else:
        reviews = Review.objects.get_all_reviews(user=user).order_by('-date')
    
    serializer = ReviewSerializer(reviews, many=True)
    return Response(serializer.data)


def search(request):
    """
    Search for reviews matching query_string
    """
    query_string = ''
    found_entries = Review.objects.all()

    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        
        found_entries = Review.objects.search_all(query_string).order_by('-date')

    response = {
        'query_string': query_string,
        'reviews': found_entries
    }

    return render(request, 'list.html', response)