from django.contrib.auth.models import User
from django.db import models

from .managers import ReviewManager
from links.models import Link

class Review(models.Model):
    title = models.CharField(max_length=2000)
    link = models.ForeignKey(Link)
    query = models.CharField(max_length=10000)
    date = models.DateTimeField()
    reviewee = models.ForeignKey(User, related_name='reviewee') # user.reviewee.all()
    reviewers = models.ManyToManyField(User, related_name='reviewers') # user.reviewers.all()
    status = models.IntegerField(default=0)
    objects = ReviewManager()

    def __unicode__(self):
        return self.title

    def is_reviewee(self, user):
    	# returns True if the user is the reviewee
    	return self.reviewee == user