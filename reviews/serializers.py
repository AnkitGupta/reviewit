from rest_framework import serializers

from django.contrib.auth.models import User

from links.models import Link
from invites.models import Invite
from comments.models import Comment
from .models import Review


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = ('id', 'title', 'link', 'query', 'date', 'reviewee', 'reviewers', 'status')
        depth = 1