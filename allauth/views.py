from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.core.urlresolvers import reverse
from django.contrib import auth

from .forms import UserCreateForm, AuthenticateForm

from invites.models import Invite

def redirect_on_invite(request, user):
	"""
	Check if a session has invite code, then add the user to the review
	and redirect to the review page
	"""
	invite = Invite.objects.get(code=request.session['code'])

	if user in invite.review.reviewers.all():
		return

	review_id = invite.review.id

	# add the user to the invite reviews' participant list
	invite.add_reviewers(user)
	del request.session['code']

	return redirect(
		reverse(
			'reviews:view', 
			args=(
				review_id,
			)
		)
	)


@csrf_protect
def register(request):
	"""
	Registers new User. If the new user is invited to a review via a code,
	the user is added to the participants list in the review and
	redirected to the review page
	"""
	if request.method == 'POST':
		form = UserCreateForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			username = cd['username']
			password = cd['password2']
			form.save()

			user = auth.authenticate(username=username, password=password)
			auth.login(request, user)
			
			if request.session.get('code'):
				redirect_on_invite(request, user)
			
			return redirect(reverse('reviews:list'))
	else:
		form = UserCreateForm()

	response = {'form': form}
	return render(request, 'register.html', response)


@csrf_protect
def login(request):
	"""
	Logins in a User and redirects to the appropriate page
	"""

	if request.user.is_authenticated():
		return redirect(reverse('reviews:index'))

	if request.method == 'POST':
		form = AuthenticateForm(data=request.POST)
		if form.is_valid():
			auth.login(request, form.get_user())

			if request.session.get('code'):
				redirect_on_invite(request, request.user)

			if request.session.get('next_page'):
				return redirect(request.session['next_page'])
			
			return redirect(reverse('reviews:list'))

	else:
		form = AuthenticateForm()

	response = {'form': form}
	return render(request, 'login.html', response)	


def logout(request):
	"""
	Logs out the user and destroys the session
	"""
	auth.logout(request)
	return redirect(reverse('reviews:index'))
