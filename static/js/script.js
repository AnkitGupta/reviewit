$(document).ready(function() 
{
    $('textarea').YellowText({
        defaultFont: 'sans-serif',
        defaultFontSize  : ".9em"
    });

    $('#comment_form').validate(
    {
        rules:
        {
            comment: "required"
        },

        submitHandler: function(form)
        {
            var $form = $(form);
            $.ajax({
                type: form.method,
                url: form.action,
                data: $form.serialize(),
                success: function(data) {
                    $('#comments_list').append(data);
                    
                    // increment comment count
                    count = $('#comment_count').text();
                    count = parseInt(count) + 1;
                    $('#comment_count').text(count);

                    form.reset();
                }
            });
        }
    });

    $('#add_friend_form').validate(
    {
        rules:
        {   
            user_email:
            {
                required: true,
                email: true
            }
        },

        submitHandler: function(form)
        {
            var $form = $(form);
            $.ajax({
                type: form.method,
                url: form.action,
                data: $form.serialize(),
                success: function(data) {
                    $('#invite_list').append(data);
                    form.reset();
                }
            });
        }

    });

    $('#ask_form').validate(
    {
        rules:
        {   
            item_title: "required",
            item_link: "required",
            item_description: "required"
        },

        submitHandler: function(form)
        {
            form.submit();
        }
    });
});