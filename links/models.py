import string
from django.db import models
from django.core.urlresolvers import reverse

from .managers import LinkManager


_char_map = string.ascii_letters+string.digits

def index_to_char(sequence):
    return "".join([_char_map[x] for x in sequence])


class Link(models.Model):

    """
    Custom URL shortner
    """

    link = models.URLField(blank=True)
    objects = LinkManager()

    def __unicode__(self):
        return "<Link : %s>"%(self.link)

    def get_short_id(self):
        _id = self.id
        digits = []
        while _id > 0:
            rem = _id % 62
            digits.append(rem)
            _id /= 62
        digits.reverse()
        return index_to_char(digits)

    @staticmethod
    def decode_id(string):
        i = 0
        for c in string:
            i = i * 62 + _char_map.index(c)
        print i
        return i

    def get_short_url(self):
        # Return short relative url
        return reverse(
            'links:view', 
            args=(
                self.get_short_id(), 
            )
        )