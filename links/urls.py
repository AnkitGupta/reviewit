from django.conf.urls import patterns, url

from links import views

urlpatterns = patterns('',
    url(r'^view/(?P<hash>[a-zA-Z0-9]+)/$', views.reverse_map_link, name='view'),
)