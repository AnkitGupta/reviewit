from django.shortcuts import get_object_or_404, redirect

from .models import Link

def reverse_map_link(request, hash):	
	"""
	Retrieve link from database matching the required hash
	"""
	_id = Link.decode_id(hash)
	link_obj = get_object_or_404(Link, id=_id)

	return redirect(link_obj.link)