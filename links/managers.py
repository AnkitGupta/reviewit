from django.db import models

class LinkManager(models.Manager):

	def create_link(self, link):
		#Creates and saves an external link
		link_obj = self.model(link=link)
		link_obj.save(using=self._db)
		return link_obj